package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {

        int n;
        int fact=1;
        int i=1;
        Scanner in = new Scanner(System.in);

        // using a while loop

        System.out.println("Factorial using a while loop");
        System.out.println("Enter a number:");
        n = in.nextInt();

        while(i<=n) {
            fact = fact*i;
            i++;
        }

        System.out.println("The factorial for " + n + " is " + fact);

        int x = 0;
        int factorial = 1;

        System.out.println("----------");
        System.out.println("Factorial using a for loop, with error handling");
        System.out.println("Enter a number");
        try {
            x = in.nextInt();
            if(x <= 0) {
                System.out.println("Number must not be zero or negative, try again");
            } else {
                for (i=2;i<=x;i++) {
                    factorial *= i;
                }
                System.out.println("The factorial for "+ x + " is " + factorial);
            }
        } catch (InputMismatchException e) {
            System.out.println("Value is not a number, try again");
        } catch (Exception e) {
            System.out.println("Invalid Input");
        }


    }

}
